defmodule Publitio.Url do
  def for(public_id, opts \\ %{}) do
    [
      prepare_asset_host(opts),
      "file",
      prepare_transformations(opts),
      prepare_folder(opts),
      public_id
    ]
    |> Enum.reject(&(is_nil(&1) or &1 == ""))
    |> Enum.join("/")
    |> append_extension(opts)
  end

  def prepare_asset_host(opts) do
    Map.get(opts, :asset_host) || "https://media.publit.io"
  end

  def prepare_transformations(opts) do
    opts
    |> Enum.map(fn
      {:width, v} -> "w_#{v}"
      {:height, v} -> "h_#{v}"
      {:crop, :fit} -> "c_fit"
      {:crop, :fill} -> "c_fill"
      {:crop, :limit} -> "c_limit"
      {:watermark, v} -> "wm_#{v}"
      {:time, v} -> "t_#{v}"
      {:start_offset, v} -> "so_#{v}"
      {:end_offset, v} -> "eo_#{v}"
      {:foreground, v} -> "fc_#{v}"
      {:background, v} -> "bc_#{v}"
      {:page, v} -> "t_#{v}"
      {:quality, v} -> "q_#{v}"
      _ -> nil
    end)
    |> Enum.reject(&is_nil(&1))
    |> Enum.join(",")
  end

  def prepare_folder(opts) do
    case opts do
      %{folder: folder} -> "#{folder}"
      _ -> nil
    end
  end

  def append_extension(url, opts) do
    case opts do
      %{extension: ext_name} -> Path.rootname(url) <> ".#{ext_name}"
      _ -> url
    end
  end
end
