defmodule Publitio.Auth do
  def generate_auth_params() do
    timestamp = current_timestamp()
    nonce = generate_nonce()

    %{
      api_key: Application.get_env(:publitio, :api_key),
      api_timestamp: timestamp,
      api_nonce: nonce,
      api_signature: sign(timestamp, nonce)
    }
  end

  defp sign(timestamp, nonce) do
    s = timestamp <> nonce <> Application.get_env(:publitio, :api_secret)
    :crypto.hash(:sha, s) |> Base.encode16(case: :lower)
  end

  defp generate_nonce() do
    Enum.random(10_000_000..99_999_999) |> Integer.to_string()
  end

  defp current_timestamp() do
    DateTime.utc_now() |> DateTime.to_unix() |> Integer.to_string()
  end
end
