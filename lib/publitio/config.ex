defmodule Publitio.Config do
  @permitted_keys ~w(
    api_key
    api_secret
  )a

  def configure(params) do
    params
    |> Keyword.take(@permitted_keys)
    |> Enum.map(fn {k, v} -> Application.put_env(:publitio, k, v) end)
  end
end
